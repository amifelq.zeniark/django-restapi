from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from . import views
from .controllers import auth, clients, servers, domains

urlpatterns = [
    path('', views.index, name='index'),
    path('auth/login',jwt_views.TokenObtainPairView.as_view(), name ='token_obtain_pair'),
    path('auth/refresh',jwt_views.TokenRefreshView.as_view(), name ='token_refresh'),
    path('login/', auth.login_view, name='login'),
    path('logout/', auth.logout_view, name='logout'),
    path('users/', views.users_list, name='get_users'),
    path('clients', clients.get_clients_list, name='get_clients'),
    path('clients/add', clients.create_client, name='create_client'),
    path('clients/<int:id>/edit', clients.update_client, name='update_client'),
    path('clients/<int:id>', clients.get_client_details, name='get_client'),
    path('clients/<int:id>/delete', clients.delete_client, name='delete_client'),
    path('servers', servers.get_servers_list, name='get_servers'),
    path('servers/add', servers.create_server, name='create_server'),
    path('servers/<int:id>/edit', servers.update_server, name='update_server'),
    path('servers/<int:id>/edit/name', servers.update_server_name, name='update_server_name'),
    path('servers/<int:id>/activity/add', servers.add_server_activity, name='add_server_activity'),
    path('servers/activity/', servers.get_server_activity_logs, name='get_server_activity_logs'),
    path('domains', domains.get_domains_list, name='get_domains'),
    path('domains/add', domains.create_domain, name='create_domain'),
    path('domains/<int:id>', domains.get_domain_details, name='get_domain'),
    path('domains/<int:id>/edit/name', domains.update_domain_name, name='update_domain_name'),
    path('domains/<int:id>/edit', domains.update_domain, name='update_domain'),
    path('domains/<int:id>/delete', domains.delete_domain, name='delete_domains'),
    path('domains/<int:id>/logs/add', domains.create_domain_logs, name='create_domain_logs'),
    path('domains/<int:id>/logs', domains.get_domain_logs, name='get_domain_logs'),
    path('domains/<int:id>/view/request', domains.view_request, name='view_domain_request'),
]