from rest_framework import serializers
from .models import Users, Clients, Domains, Logs, Servers, ServerAcitvityLogs

class UsersSerializer(serializers.ModelSerializer):
  class Meta:
    model = Users
    fields = ['user_id', 'username', 'password', 'last_logged_in', 'access']

class ClientsSerializer(serializers.ModelSerializer):
  class Meta:
    model = Clients
    fields = ['client_id', 'added_by_id', 'client_name', 'status', 'date_added', 'date_updated']

class ServersSerializer(serializers.ModelSerializer):
  class Meta:
    model = Servers
    fields = ['server_id', 'server_name', 'added_by_id', 'server_ip_address', 'description', 'status', 'date_added', 'date_updated']

class ServerActivityLogsSerializer(serializers.ModelSerializer):
  class Meta:
    model = ServerAcitvityLogs
    fields = ['log_id', 'added_by_id', 'server_id', 'activity', 'date_added']

class DomainsSerializer(serializers.ModelSerializer):
  class Meta:
    model = Domains
    fields = ['domain_id', 'client_id', 'added_by_id', 'domain_name', 'domain_host', 'username', 'password', 'notes', 'status', 'date_added', 'date_updated']

class LogsSerializer(serializers.ModelSerializer):
  class Meta:
    model = Logs
    fields = ['log_id', 'user_id', 'domain_id', 'logs', 'date_added']

