from django.http import JsonResponse
from django.contrib.auth.models import User
from datetime import datetime
from rest_framework.decorators import api_view, permission_classes
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from zeniarkdev.models import Clients, Status, Domains, Logs, ActivityLogMessages, ActivityLogs
from zeniarkdev.serializers import DomainsSerializer, LogsSerializer


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_domains_list(request):
    domains = Domains.objects.all().filter(status=Status.ACTIVE)
    serializer = DomainsSerializer(domains, many=True)
    return JsonResponse({'message': 'Domains/subdomains successfully retrieved', 'data': serializer.data, 'count': len(serializer.data), 'status_code': 200}, status=200)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_domain_details(request, id):
    try:
        # get domain details
        domain = Domains.objects.get(domain_id=id, status=Status.ACTIVE)
    except Domains.DoesNotExist:
        return JsonResponse({'message': "Domain/subdomain not found", 'status_code': 404}, status=404)
    domainserializer = DomainsSerializer(domain, many=False)

    # get logs
    logs = Logs.objects.all().filter(domain_id=domain)
    logserializer = LogsSerializer(logs, many=True)

    # TODO: map logs, get username of user_id
    return JsonResponse({'message': 'Domain/subdomain details successfully retrieved', 'data': {'domain': domainserializer.data, 'logs': logserializer.data}, 'status_code': 200}, status=200)


@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def create_domain(request):
    # TODO: Better validation
    if request.data['client_id'] is None:
        return JsonResponse({'message': "Client ID is required", 'status_code': 400}, status=400)
    try:
        client = Clients.objects.get(
            client_id=request.data['client_id'], status=Status.ACTIVE)
    except Clients.DoesNotExist:
        return JsonResponse({'message': "Client not found", 'status_code': 404}, status=404)
    user = User.objects.get(id=request.user.id)
    domain = Domains(domain_name=request.data['name'], domain_host=request.data['host'], username=request.data['username'],
                     password=request.data['password'], notes=request.data['notes'], client_id=client, status=Status.ACTIVE, date_added=datetime.now(), added_by=user)
    domain.save()

    # Add log
    log_message = ActivityLogMessages.objects.filter(module_name='add_domain').first()
    ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity').format(domain_name=domain.domain_name, client_name=client.client_name), module=log_message, date_added=datetime.now(), params=str({'domain_name': domain.domain_name, 'client_name': client.client_name}))

    serializer = DomainsSerializer(domain, many=False)
    return JsonResponse({'message': 'Domain successfully added', 'data': serializer.data, 'status_code': 200}, status=200)


@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def update_domain(request, id):
    user = User.objects.get(id=request.user.id)

    try:
        domain = Domains.objects.get(domain_id=id, status=Status.ACTIVE)
    except Domains.DoesNotExist:
        return JsonResponse({'message': "Domain not found", 'status_code': 404}, status=404)
    old_domain = domain
    domain.domain_name = request.data['name']
    domain.domain_host = request.data['host']
    domain.username = request.data['username']
    domain.password = request.data['password']
    domain.notes = request.data['notes']
    domain.date_updated = datetime.now()
    domain.save()
    serializer = DomainsSerializer(domain, many=False)

    client = Clients.objects.get(client_id=domain.client_id)

    if domain.domain_name != old_domain.domain_name:
        # Add log
        log_message = ActivityLogMessages.objects.filter(module_name='edit_domain').first()
        activity_text = getattr(log_message, 'activity').format(old_domain_name=old_domain.domain_name, new_domain_name=domain.domain_name, client_name=client.client_name)
        ActivityLogs.objects.create(user=user, activity=activity_text, module=log_message, date_added=datetime.now(), params=str({'old_domain_name': old_domain.domain_name, 'new_domain_name': domain.domain_name,'client_name': client.client_name}))

    return JsonResponse({'message': 'Domain successfully updated', 'data': serializer.data, 'status_code': 200}, status=200)

@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def update_domain_name(request, id):
    user = User.objects.get(id=request.user.id)

    try:
        domain = Domains.objects.get(domain_id=id, status=Status.ACTIVE)
    except Domains.DoesNotExist:
        return JsonResponse({'message': "Domain not found", 'status_code': 404}, status=404)
    old_domain = domain
    domain.domain_name = request.data['name']
    domain.date_updated = datetime.now()
    domain.save()
    serializer = DomainsSerializer(domain, many=False)

    client = Clients.objects.get(client_id=domain.client_id)

    if domain.domain_name != old_domain.domain_name:
        # Add log
        log_message = ActivityLogMessages.objects.filter(module_name='edit_domain').first()
        activity_text = getattr(log_message, 'activity').format(old_domain_name=old_domain.domain_name, new_domain_name=domain.domain_name, client_name=client.client_name)
        ActivityLogs.objects.create(user=user, activity=activity_text, module=log_message, date_added=datetime.now(), params=str({'old_domain_name': old_domain.domain_name, 'new_domain_name': domain.domain_name,'client_name': client.client_name}))

    return JsonResponse({'message': 'Domain name successfully updated', 'data': serializer.data, 'status_code': 200}, status=200)

@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def view_request(request, id):
    user = User.objects.get(id=request.user.id)

    try:
        domain = Domains.objects.get(domain_id=id, status=Status.ACTIVE)
    except Domains.DoesNotExist:
        return JsonResponse({'message': "Domain not found", 'status_code': 404}, status=404)

    # Add log
    log_message = ActivityLogMessages.objects.filter(module_name='view_domain').first()
    activity_text = getattr(log_message, 'activity').format(domain_name=domain.domain_name, purpose=request.data['purpose'])
    ActivityLogs.objects.create(user=user, activity=activity_text, module=log_message, date_added=datetime.now(), params=str({'domain_name': domain.domain_name, 'purpose': request.data['purpose']}))

    return JsonResponse({'message': 'Domain view request successfully saved', 'data': request.data['purpose'], 'status_code': 200}, status=200)

@api_view(['DELETE'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def delete_domain(request, id):
    user = User.objects.get(id=request.user.id)

    try:
        domain = Domains.objects.get(domain_id=id, status=Status.ACTIVE)
    except Domains.DoesNotExist:
        return JsonResponse({'message': "Domain not found", 'status_code': 404}, status=404)
    domain.status = Status.REMOVED
    domain.date_updated = datetime.now()
    domain.save()

    client = Clients.objects.get(client_id=domain.client_id)

    # Add log
    log_message = ActivityLogMessages.objects.filter(module_name='delete_domain').first()
    activity_text = getattr(log_message, 'activity').format(domain_name=domain.domain_name, client_name=client.client_name)
    ActivityLogs.objects.create(user=user, activity=activity_text, module=log_message, date_added=datetime.now(), params=str({'domain_name': domain.domain_name, 'client_name': client.client_name}))

    return JsonResponse({'message': 'Domain successfully deleted', 'status_code': 200}, status=200)


@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def create_domain_logs(request, id):
    try:
        domain = Domains.objects.get(domain_id=id, status=Status.ACTIVE)
    except Domains.DoesNotExist:
        return JsonResponse({'message': "Domain not found", 'status_code': 404}, status=404)
    user = User.objects.get(id=request.user.id)
    
    log = Logs.objects.create(uid=user, domain_id=domain, logs=request.data['notes'], date_added=datetime.now())
    serializer = LogsSerializer(log, many=False)

    return JsonResponse({'message': 'Logs successfully added', 'data': serializer.data, 'status_code': 200}, status=200)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_domain_logs(request, id):
    try:
        domain = Domains.objects.get(domain_id=id, status=Status.ACTIVE)
    except Domains.DoesNotExist:
        return JsonResponse({'message': "Domain not found", 'status_code': 404}, status=404)
    logs = Logs.objects.all().filter(domain_id=domain)
    serializer = LogsSerializer(logs, many=True)
    return JsonResponse({'message': 'Logs successfully retrieved', 'data': serializer.data, 'count': len(serializer.data), 'status_code': 200}, status=200)
