import re
from xmlrpc.client import Server
from django.http import JsonResponse
from django.contrib.auth.models import User
from datetime import datetime
from rest_framework.decorators import api_view, permission_classes
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from zeniarkdev.models import Servers, Status, ActivityLogMessages, ActivityLogs, ServerAcitvityLogs
from zeniarkdev.serializers import ServersSerializer, ServerActivityLogsSerializer


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_servers_list(request):
    servers = Servers.objects.all().filter(status=Status.ACTIVE)
    serializer = ServersSerializer(servers, many=True)
    return JsonResponse({'message': 'Servers successfully retrieved', 'data': serializer.data, 'count': len(serializer.data), 'status_code': 200}, status=200)


@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def create_server(request):
    user = User.objects.get(id=request.user.id)

    server = Servers.objects.create(server_name=request.data['name'], server_ip_address=request.data['ip_address'], description=request.data['description'], status=Status.ACTIVE, date_added=datetime.now(), added_by=user)
    serializer = ServersSerializer(server, many=False)

    # Add log
    log_message = ActivityLogMessages.objects.filter(module_name='add_server').first()
    ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity').format(server_name=server.server_name), module=log_message, date_added=datetime.now(), params=str({'server_name': server.server_name}))

    return JsonResponse({'message': 'Server successfully added', 'data': serializer.data, 'status_code': 200}, status=200)

@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def update_server(request, id):
    user = User.objects.get(id=request.user.id)

    try:
        server = Servers.objects.get(server_id=id, status=Status.ACTIVE)
    except Servers.DoesNotExist:
        return JsonResponse({'message': "Server not found", 'status_code': 404}, status=404)
    old_server = server
    server.server_name = request.data['name']
    server.server_ip_address = request.data['ip_address']
    server.description = request.data['description']
    server.save()

    # Add log
    if old_server.server_name != server.server_name:
        log_message = ActivityLogMessages.objects.filter(module_name='edit_server').first()
        ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity').format(old_server_name=old_server.server_name, new_server_name=server.server_name), module=log_message, date_added=datetime.now(), params=str({'old_server_name': old_server.server_name, 'new_server_name': server.server_name}))

    serializer = ServersSerializer(server, many=False)
    return JsonResponse({'message': 'Server successfully updated', 'data': serializer.data, 'status_code': 200}, status=200)

@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def update_server_name(request, id):
    user = User.objects.get(id=request.user.id)

    try:
        server = Servers.objects.get(server_id=id, status=Status.ACTIVE)
    except Servers.DoesNotExist:
        return JsonResponse({'message': "Server not found", 'status_code': 404}, status=404)
    old_server = server
    server.server_name = request.data['name']
    server.save()

    # Add log
    log_message = ActivityLogMessages.objects.filter(module_name='edit_server').first()
    ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity').format(old_server_name=old_server.server_name, new_server_name=server.server_name), module=log_message, date_added=datetime.now(), params=str({'old_server_name': old_server.server_name, 'new_server_name': server.server_name}))

    serializer = ServersSerializer(server, many=False)
    return JsonResponse({'message': 'Server name successfully updated', 'data': serializer.data, 'status_code': 200}, status=200)

@api_view(['DELETE'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def delete_server(request, id):
    user = User.objects.get(id=request.user.id)

    try:
        server = Servers.objects.get(server_id=id, status=Status.ACTIVE)
    except Servers.DoesNotExist:
        return JsonResponse({'message': "Server not found", 'status_code': 404}, status=404)
    server.status = Status.REMOVED
    server.date_updated = datetime.now()
    server.save()

    # Add log
    log_message = ActivityLogMessages.objects.filter(module_name='delete_server').first()
    ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity').format(server_name=server.server_name), module=log_message, date_added=datetime.now(), params=str({'server_name': server.server_name}))

    return JsonResponse({'message': 'Server successfully deleted', 'status_code': 200}, status=200)

# SERVER ACTIVITY LOGS

@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def add_server_activity(request, id):
    user = User.objects.get(id=request.user.id)

    try:
        server = Servers.objects.get(server_id=id, status=Status.ACTIVE)
    except Servers.DoesNotExist:
        return JsonResponse({'message': "Server not found", 'status_code': 404}, status=404)

    server_activity = ServerAcitvityLogs.objects.create(server=server, activity=request.data['activity'], date_added=datetime.now(), added_by=user)
    serializer = ServerActivityLogsSerializer(server_activity, many=False)

    return JsonResponse({'message': 'Server activity successfully added', 'data': serializer.data, 'status_code': 200}, status=200)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_server_activity_logs(request):
    options = {}
    # TODO: Fix search query
    search = ''
    if request.query_params.get('server_id') is not None:
        options['server'] = request.query_params.get('server_id')
    if request.query_params.get('search') is not None:
        search = request.query_params.get('search')
    search = rf"\b(?=\w){search}\b(?!\w)"
    print(search)
    server_activities = ServerAcitvityLogs.objects.all().filter(**options, activity=search)

    # TODO: map added_by_id to username, extend query search to username

    serializer = ServerActivityLogsSerializer(server_activities, many=True)
    return JsonResponse({'message': 'Server activities successfully retrieved', 'data': serializer.data, 'count': len(serializer.data), 'status_code': 200}, status=200)
