from django.http import JsonResponse
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from datetime import datetime
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken, BlacklistedToken
from rest_framework.decorators import api_view, permission_classes
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework_simplejwt.tokens import RefreshToken
from zeniarkdev.models import ActivityLogMessages, ActivityLogs
# @api_view(['POST'])
# @parser_classes([JSONParser])
# @permission_classes([AllowAny])
# def login_view(request):
#     try:
#         user = authenticate(request, username=request.data['username'], password=request.data['password'])
#         if user is not None:
#             req = login(request, user)
#             print(req)
#             return JsonResponse({'message': "Login success", 'status_code': 200}, status=200);
#         return JsonResponse({'message': "Login failed", 'status_code': 400}, status=400);
#     except:
#         return JsonResponse({'message': "Login failed", 'status_code': 400}, status=400);

# @api_view(['POST'])
# def logout_view(request):
#     try:
#         logout(request)
#     except:
#         return JsonResponse({'message': 'Log out failed', 'status_code': 400}, status=400)
#     return JsonResponse({'message': 'Log out success', 'status_code': 200}, status=200)


@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([AllowAny])
def login_view(request):
    # try:
        user = authenticate(
            request, username=request.data['username'], password=request.data['password'])
        if user is not None:
            refresh = RefreshToken.for_user(user)
            login(request, user)
            tokens = {
                'refresh': str(refresh),
                'access': str(refresh.access_token),
            }
            request.session['refresh'] = str(refresh)
            log_message = ActivityLogMessages.objects.filter(module_name='login').first()
            ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity'), module=log_message, date_added=datetime.now())
            return JsonResponse({'message': "Login success", "data": tokens, 'status_code': 200}, status=200)
        return JsonResponse({'message': "Login failed", 'status_code': 400}, status=400)
    # except:
    #     return JsonResponse({'message': "Login failed", 'status_code': 400}, status=400)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def logout_view(request):
    # try:
    user = User.objects.get(id=request.user.id)
    token: OutstandingToken
    for token in OutstandingToken.objects.filter(user=request.user):
      print(token)
      _, _ = BlacklistedToken.objects.get_or_create(token=token)
    session_token = RefreshToken(request.session['refresh'])
    if session_token is not None:
      token = RefreshToken(session_token)
      token.blacklist()
    logout(request)

    log_message = ActivityLogMessages.objects.filter(module_name='logout').first()
    ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity'), module=log_message, date_added=datetime.now())
    return JsonResponse({'message': "Logout success", 'status_code': 200}, status=200)
    # except:
    #     return JsonResponse({'message': 'Log out failed', 'status_code': 400}, status=400)
