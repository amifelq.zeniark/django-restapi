from django.http import JsonResponse
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from datetime import datetime
from rest_framework.decorators import api_view, permission_classes
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from zeniarkdev.models import Clients, Status, ActivityLogMessages, ActivityLogs
from zeniarkdev.serializers import ClientsSerializer


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_clients_list(request):
    clients = Clients.objects.all().filter(status=Status.ACTIVE)
    serializer = ClientsSerializer(clients, many=True)
    return JsonResponse({'message': 'Clients successfully retrieved', 'data': serializer.data, 'count': len(serializer.data), 'status_code': 200}, status=200)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_client_details(request, id):
    try:
        client = Clients.objects.get(client_id=id, status=Status.ACTIVE)
    except Clients.DoesNotExist:
        return JsonResponse({'message': "Client not found", 'status_code': 404}, status=404)
    serializer = ClientsSerializer(client, many=False)
    return JsonResponse({'message': 'Client details successfully retrieved', 'data': serializer.data, 'status_code': 200}, status=200)


@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def create_client(request):
  user = User.objects.get(id=request.user.id)
  # TODO: Better validation
  if request.data['client_name'] is None:
    return JsonResponse({'message': "Client name is required", 'status_code': 400}, status=400)
  user = User.objects.get(id=request.user.id)
  client = Clients(client_name=request.data['client_name'], added_by=user, status=Status.ACTIVE, date_added=datetime.now())
  client.save()

  # Add log
  log_message = ActivityLogMessages.objects.filter(module_name='add_client').first()
  ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity').format(client_name=client.client_name), module=log_message, date_added=datetime.now(), params=str({'client_name': client.client_name}))

  serializer = ClientsSerializer(client, many=False)
  return JsonResponse({'message': 'Client successfully added', 'data': serializer.data, 'status_code': 200}, status=200)


@api_view(['POST'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def update_client(request, id):
  user = User.objects.get(id=request.user.id)

  try:
    # TODO: Better validation
    client = Clients.objects.get(client_id=id, status=Status.ACTIVE)
    old_client = client
  except Clients.DoesNotExist:
    return JsonResponse({'message': "Client not found", 'status_code': 404}, status=404)
  if request.data['client_name'] is None:
      return JsonResponse({'message': "Client name is required", 'status_code': 400}, status=400)
  client.client_name = request.data['client_name']
  client.date_updated = datetime.now()
  client.save()

  # Add log
  log_message = ActivityLogMessages.objects.filter(module_name='edit_client').first()
  ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity').format(old_client_name=old_client.client_name, new_client_name=client.client_name), module=log_message, date_added=datetime.now(), params=str({'old_client_name': old_client.client_name, 'new_client_name': client.client_name}))

  serializer = ClientsSerializer(client, many=False)
  return JsonResponse({'message': 'Client successfully updated', 'data': serializer.data, 'status_code': 200}, status=200)


@api_view(['DELETE'])
@parser_classes([JSONParser])
@permission_classes([IsAuthenticated])
def delete_client(request, id):
  user = User.objects.get(id=request.user.id)

  try:
      client = Clients.objects.get(client_id=id, status=Status.ACTIVE)
  except Clients.DoesNotExist:
      return JsonResponse({'message': "Client not found", 'status_code': 404}, status=404)
  client.status = Status.REMOVED
  client.date_updated = datetime.now()
  client.save()

  # Add log
  log_message = ActivityLogMessages.objects.filter(module_name='delete_client').first()
  ActivityLogs.objects.create(user=user, activity=getattr(log_message, 'activity').format(client_name=client.client_name), module=log_message, date_added=datetime.now(), params=str({'client_name': client.client_name}))

  return JsonResponse({'message': 'Client successfully deleted', 'status_code': 200}, status=200)
