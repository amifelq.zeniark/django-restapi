# <project>/<app>/management/commands/seed.py
from datetime import datetime
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from zeniarkdev.models import ActivityLogMessages
import logging

logger = logging.getLogger()

ACTIVITY_LOGS = {
  'login': 'Logged In',
  'logout': 'Logged out',
  'add_client': 'Added client {client_name}',
  'edit_client': 'Edited client {old_client_name} to {new_client_name}',
  'delete_client': 'Removed client {client_name}',
  'add_domain': 'Added {domain_name} domain to {client_name} client',
  'edit_domain': 'Edited {old_domain_name} domain to {new_domain_name} from {client_name} client',
  'delete_domain': 'Removed {domain_name} domain from {client_name} client',
  'view_domain': 'Viewed {domain_name} FTP details for: {purpose}',
  'add_server': 'Added server {server_name}',
  'edit_server': 'Edited server {old_server_name} to {new_server_name}',
  'delete_server': 'Removed server {server_name}',
}
# python manage.py seed --mode=refresh

""" Clear all data and creates addresses """
MODE_REFRESH = 'refresh'

""" Clear all data and do not create any object """
MODE_CLEAR = 'clear'

class Command(BaseCommand):
    help = "seed database for testing and development."

    def add_arguments(self, parser):
        parser.add_argument('--mode', type=str, help="Mode")

    def handle(self, *args, **options):
        self.stdout.write('seeding data...')
        run_seed(self, options['mode'])
        self.stdout.write('done.')


def clear_data():
    """Deletes all the inserted data"""
    logger.info("Deleting test user")
    try:
      User.objects.get(username='testuser').delete()
    except User.DoesNotExist:
      logger.info("User does not exist")
    logger.info("Deleting test testadmin")
    try:
      User.objects.get(username='testadmin', is_superuser=True).delete()
    except User.DoesNotExist:
      logger.info("Superuser does not exist")
    logger.info("Deleting activity log messages")
    ActivityLogMessages.objects.filter(module_name__in=list(ACTIVITY_LOGS.values())).delete()

# Create CRUD permissions for clients, domains and servers
# def create_permissions():
  

# Create user groups: admin, users
# def create_user_groups():
#   content_type = ContentType.objects.get_for_model(Clients)
#   permission = Permission.objects.create(
#     codename='can_add_client',
#     name='Can add clients',
#     content_type=content_type,
#   )

def create_user():
  logger.info("Creating user")
  user = User.objects.create_user('testuser', 'test@email.com', 'user')
  user.save()
  logger.info("User {} created.".format(user.username))

def create_super_user():
  logger.info("Creating superuser")
  user = User.objects.create_superuser('testadmin', 'test@email.com', 'admin')
  user.save()
  logger.info("Superuser {} created.".format(user.username))

def create_activity_log_messages():
  module_names = list(ACTIVITY_LOGS.keys())
  ctr = 0
  for activity in list(ACTIVITY_LOGS.values()):
    logger.info("Creating activity")
    ActivityLogMessages.objects.create(activity=activity, module_name=module_names[ctr], date_added=datetime.now())
    logger.info("{} activity created.".format(module_names[ctr]))
    ctr += 1

def run_seed(self, mode):
    """ Seed database based on mode

    :param mode: refresh / clear 
    :return:
    """
    # Clear data from tables
    clear_data()
    if mode == MODE_CLEAR:
      return

    create_super_user()
    create_user()
    create_activity_log_messages()