from django.apps import AppConfig


class ZeniarkdevConfig(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'zeniarkdev'
