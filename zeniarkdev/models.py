from math import fabs
from xmlrpc.client import Server
from django.db import models
from django.contrib.auth.models import User

class Status(models.IntegerChoices):
  ACTIVE = 1
  REMOVED = 2

class Users(models.Model):
  class AccessLevel(models.IntegerChoices):
    ADMIN = 1
    USER = 2
  user_id = models.AutoField(primary_key=True)
  username = models.CharField(max_length=50, unique=True)
  password = models.CharField(max_length=50)
  last_logged_in = models.DateTimeField(null=True)
  access = models.IntegerField(AccessLevel.choices)
  current_token = models.CharField(max_length=250, null=True)
  
  class Meta: 
    ordering = ['user_id']

class Clients(models.Model):
  client_id= models.AutoField(primary_key=True)
  added_by= models.ForeignKey(User, on_delete=models.DO_NOTHING)
  client_name= models.CharField(max_length=50, null=False)
  status= models.IntegerField(choices=Status.choices)
  date_added= models.DateTimeField()
  date_updated= models.DateTimeField(null=True)

class Servers(models.Model):
  server_id= models.AutoField(primary_key=True)
  added_by= models.ForeignKey(User, on_delete=models.DO_NOTHING)
  server_name= models.CharField(max_length=50, null=False, unique=True)
  server_ip_address= models.CharField(max_length=50, null=False, unique=True)
  description= models.TextField(null=True)
  status= models.IntegerField(choices=Status.choices)
  date_added= models.DateTimeField()
  date_updated= models.DateTimeField(null=True)

class ServerAcitvityLogs(models.Model):
  log_id= models.AutoField(primary_key=True)
  added_by= models.ForeignKey(User, on_delete=models.DO_NOTHING)
  server= models.ForeignKey(Servers, on_delete=models.CASCADE)
  activity= models.TextField(max_length=250)
  date_added= models.DateTimeField()
  
class Domains(models.Model):
  domain_id= models.AutoField(primary_key=True)
  client= models.ForeignKey(Clients, on_delete=models.CASCADE )
  added_by= models.ForeignKey(User, on_delete=models.DO_NOTHING)
  server= models.ForeignKey(Servers, on_delete=models.CASCADE)
  domain_name= models.CharField(max_length=50, null=False)
  domain_host= models.CharField(max_length=100, null=False)
  username= models.CharField(max_length=50, null=False)
  notes= models.TextField(null=True)
  password= models.TextField()
  status=  models.IntegerField(choices=Status.choices)
  date_added= models.DateTimeField()
  date_updated= models.DateTimeField(null=True)

class Logs(models.Model):
  log_id= models.AutoField(primary_key=True)
  user= models.ForeignKey(User, on_delete=models.DO_NOTHING)
  domain= models.ForeignKey(Domains, on_delete=models.CASCADE)
  logs= models.TextField(null=False)
  date_added= models.DateTimeField()

class ActivityLogMessages(models.Model):
  module_id= models.AutoField(primary_key=True)
  module_name= models.TextField(max_length=50, null=True)
  activity= models.TextField(null=False)
  date_added= models.DateTimeField()
  date_updated= models.DateTimeField(null=True)

class ActivityLogs(models.Model):
  log_id= models.AutoField(primary_key=True)
  user= models.ForeignKey(User, on_delete=models.CASCADE)
  module= models.ForeignKey(ActivityLogMessages, on_delete=models.DO_NOTHING)
  activity= models.TextField(null=False)
  params= models.TextField(null=False)
  date_added= models.DateTimeField()
