from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from .models import Users
from .serializers import UsersSerializer

def index(request):
    return HttpResponse("Hello, world")
 
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def users_list(request):
    if request.user.is_superuser:
        users = Users.objects.all()
        serializer = UsersSerializer(users, many=True)
        return JsonResponse(serializer.data, safe=False)
    else:
        return JsonResponse({'message': "Unauthorized access", 'status_code': 401}, status=401)

