# django-restapi



## Description

API built in Django & Django REST framework. Uses Django auth system & MySQL for database.

## Virtual Environment

Read more here: https://docs.python.org/3/tutorial/venv.html
- After environment is created, pull project inside directory
- Activate virtual environment first before proceeding to `Setup`
```
source bin/activate
```
- Change to app directory

## Setup

1. Install project dependencies

```
pip install -r requirements.txt
```

2. Create database, change credentials in `DATABASES` in `app/settings.py`

3. Run migrations

```
python manage.py migrate 
```

3. Seed database

```
python manage.py seed 
```

4. Run server

```
python manage.py runserver
```

## Todo

- [ ] Middleware for checking JWT if valid 
- [x] CRUD for servers
- [ ] Create and read for activity logs (Create done)
- [x] Create server activity model
- [ ] Create and read for server activity
- [ ] Validations for requests
- [ ] Squash migrations
- [x] Seed users, super users and activity log messages